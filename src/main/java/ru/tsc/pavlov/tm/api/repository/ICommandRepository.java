package ru.tsc.pavlov.tm.api.repository;

import ru.tsc.pavlov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
