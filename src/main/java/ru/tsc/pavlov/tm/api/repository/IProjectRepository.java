package ru.tsc.pavlov.tm.api.repository;

import ru.tsc.pavlov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    boolean existsById(String id);

    boolean existsByIndex(int index);

    boolean existsByName(String name);

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(int index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(int index);

}
