package ru.tsc.pavlov.tm.repository;

import ru.tsc.pavlov.tm.api.repository.IProjectRepository;
import ru.tsc.pavlov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(Project project) {
        projects.add(project);
    }

    @Override
    public void remove(Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public boolean existsById(String id) {
        return findById(id) != null;
    }

    @Override
    public boolean existsByIndex(int index) {
        return index < projects.size() && index >= 0;
    }

    @Override
    public boolean existsByName(String name) {
        return findByName(name) != null;
    }

    @Override
    public Project findById(String id) {
        for (Project project : projects)
            if (id.equals(project.getId())) return project;
        return null;

    }

    @Override
    public Project findByName(String name) {
        for (Project project : projects)
            if (name.equals(project.getName())) return project;
        return null;
    }

    @Override
    public Project findByIndex(int index) {
        return projects.get(index);
    }

    @Override
    public Project removeById(String id) {
        final Project project = findById(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByName(String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

}
